/// <reference path="jquery-3.6.0.js" />

let listChose = [];
$("#btn1").on("click", (() => {
    const html = $("#tmpHome").html();
    $("#main").html(html);
    listChose = [];
}));
$("#btn2").on("click", (() => {
    const html = $("#tmpLive").html();
    $("#main").html(html);
    if (listChose == "") {
        alert("There are no selected currencies")
        $("#main").html("");
    }
    else
        $("#report").html(listChose);
}));
$("#btn3").on("click", (() => {
    const html = $("#tmpAbout").html();
    $("#main").html(html);
}));

$("#btn1").on("click", (function () {
    $.ajax({
        method: "GET",
        url: "https://api.coingecko.com/api/v3/coins/list",
        success: function (list) {
            show(list);
        },
        error: e => alert(`Error in ajax request: ${e.status}`)
    });
})
);
function show(list) {
    for (let i = 0; i < list.length; i++) {
        const element = list[i];
        $("#result").append(`<div id="${element.symbol}" class="container">
        <div id="${element.id}" class="card" style="width:350px"> <div class="card-header">${element.symbol}<div class="custom-control custom-switch">
        <input type="checkbox" id="chec${element.id}" class="custom-control-input"/>
        <label class="custom-control-label" for="chec${element.id}"></label></div>
        <div class="card-body">${element.name}</div>
        <div class="card-footer">
        <button  type="button" class="btn btn-primary" data-toggle="collapse" data-target="#info${element.id}" id="btn${element.id}">more info</button> 
         <br> <div class="collapse" id="info${element.id}"></div></div>
        </div>
      </div>`);


        $(`#chec${element.id}`).on("click", () => {
            let coin = `${element.name}`;
            if (listChose.includes(`${coin}`)) {
                listChose.splice(listChose.indexOf(coin), 1);
            }
            else
                if (listChose.length > 4) {
                    $(`#chec${element.id}`).prop("checked", false);
                    $("#Mcontent").html("");
                    for (let g = 0; g < listChose.length; g++) {
                        const coin_c = listChose[g];
                        $("#Mcontent").append(`<div class="custom-control custom-switch">
                    <div><input type="checkbox" class="custom-control-input" id="change${coin_c}" checked><br>
                    <label class="custom-control-label" for="change${coin_c}">${coin_c}</label></div>
                  </div>`);
                        $(`#change${coin_c}`).on("click", () => {
                            listChose.splice(listChose.indexOf(`${coin_c}`), 1);
                            listChose.push(`${element.name}`)
                            $(`#chec${coin_c}`).prop("checked", false);
                            $(`#chec${element.id}`).prop("checked", true);
                            $("#modal").modal('hide');
                        });
                    }
                    $("#modal").modal('show');
                }
                else
                    listChose.push(coin);
        });


        $(`#btn${element.id}`).on("click", (function () {
            if (localStorage.getItem(element.id) != undefined) {
                showInfo(JSON.parse(localStorage.getItem(element.id)));
            }
            else {
                prog()
                $.ajax({
                    method: "GET",
                    url: `https://api.coingecko.com/api/v3/coins/${element.id}`,
                    success: function (info) {
                        showInfo(info);
                        localStorage.setItem(element.id, JSON.stringify(info));
                        setTimeout(deleteCoinInfo, 120000, element.id);
                    },
                    error: e => alert(`Error in ajax request: ${e.status}`)
                });
            }

        }));
    }
}
function showInfo(info) {
    $(`#info${info.id}`).html("")
    $(`#info${info.id}`).append(`${info.market_data.current_price.usd}$ <br>
    ${info.market_data.current_price.eur}&euro;<br>
    ${info.market_data.current_price.ils}&#8362;<br>
    <img src="${info.image.small}" >`);
}
$(document).ajaxSend(() => {
    $(".loader").css("display", "block");
}).ajaxComplete(() => {
    $(".loader").css("display", "none");
}); 

function prog() {
    let p = 0;
    const interval = setInterval(() => {
        p += 10;
        if (p <= 100) {
            $("#rate").width(`${p}%`);
            $("#rate").text(`${p}%`);
        }
        else
            clearInterval(interval);
    }, 15);
    
}
$("#btnSearch").on("click", () => {
    let symbol = $("#search").val();
    $("#search").val("");
    if (symbol == "") alert("you mast insert value");
    let coin = $(`div[id=${symbol}]`);
    if (coin.length > 0) {
        let coinid = coin[0].childNodes[1].id;
        $.ajax({
            method: "GET",
            url: `https://api.coingecko.com/api/v3/coins/${coinid}`,
            success: function (coin) {
                $("#result").html("")
                let list1 = [];
                list1.push(coin);
                show(list1);
            },
            error: e => alert(`Error in ajax request: ${e.status}`)
        });
    }
    else { alert("not found") }
});
function deleteCoinInfo(coinId) {
    localStorage.removeItem(coinId);
}



